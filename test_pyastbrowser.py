import pyastbrowser


def dummy_dispnode(s, i):
    return pyastbrowser.DispNode(
        pos=(i, i, i, i), label=s, name=s, inline_attrs={s: s}, children=[],
    )


def dummy_disptree():
    n1 = dummy_dispnode('a', 1)
    n2 = dummy_dispnode('b', 2)
    n3 = dummy_dispnode('c', 3)
    n1.children.append(n2)
    n2.children.append(n3)
    n2.parent = n1
    n3.parent = n2
    return n1, n2, n3


def test_get_color_string():
    assert pyastbrowser.get_color_string(0, 127, 255).lower() == '#007fff'


def test_colorhash():
    def test(text, luma):
        colorhash = pyastbrowser.get_colorhash(text, luma)
        return sum(colorhash)

    color1 = test('hello', 0)
    color2 = test('hello', 0.5)
    color3 = test('hello', 1)
    assert color1 < color2 < color3


def test_dispnode_adjust_parents():
    n1, n2, n3 = dummy_disptree()
    n3.children[:] = [n2]
    n2.children[:] = [n1]
    n1.children[:] = []
    n3.adjust_parents()
    assert n1.parent is n2
    assert n2.parent is n3
    assert n3.parent is n2, "The root's parent must be preserved"


def test_dispnode_get_path_label():
    n1, n2, n3 = dummy_disptree()
    assert n3.get_path_label() == 'abc'


def test_get_config_basedirs():
    # Just test that it runs
    assert all(pyastbrowser.get_config_basedirs())


def test_get_config_paths():
    from pathlib import Path

    d = Path(__file__).parent
    paths = list(pyastbrowser.get_config_paths([d / "a", d / "b"]))
    assert paths == [d / "a/pyastbrowser/config.ini", d / "b/pyastbrowser/config.ini"]


def test_open_any():
    from pathlib import Path
    import pytest

    p = Path(__file__)
    d = p.parent
    pn = d / "$$ nonexistent file $$"
    with pyastbrowser.open_any([p, pn]) as f:
        assert Path(f.name) == p
    with pyastbrowser.open_any([pn, p]) as f:
        assert Path(f.name) == p
    with pytest.raises(FileNotFoundError):
        with pyastbrowser.open_any([pn]):
            pass


def test_read_config():
    import io

    f = io.StringIO()
    f.write("[a.b]\nc = {'d': ['e', 'f']}")
    f.seek(0)
    config = pyastbrowser.read_config(f)
    print(config)
    assert config['a.b.c'] == {'d': ['e', 'f']}
