#!/usr/bin/env python3

# Python AST Browser
# Copyright 2020 Johannes Sasongko <sasongko@gmail.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from __future__ import annotations

__version__ = "0.0"
__all__ = ['AstBrowser', 'run']

import ast
from configparser import ConfigParser
from dataclasses import dataclass
import os
from pathlib import Path
import sys
import tkinter
from tkinter import ttk
from typing import (
    Any,
    Callable,
    ClassVar,
    Dict,
    Final,
    Iterable,
    Iterator,
    List,
    Optional,
    TextIO,
    Tuple,
    Union,
)


APPNAME: Final = "Python AST Browser"

# List of AST node classes to be highlighted in the treeview.
# This should mainly contain statements, plus a few expressions that benefit
# from increased visibility, for example AugAssign and Call.
HIGHLIGHT_NODES: Final = [
    # fmt: off
    'AnnAssign', 'Assert', 'Assign', 'AsyncFor', 'AsyncFunctionDef', 'AsyncWith', 'AugAssign',
    'Break',
    'Call', 'ClassDef', 'Continue',
    'Delete',
    'ExceptHandler', 'Expr',
    'For', 'FunctionDef',
    'Global',
    'If', 'Import', 'ImportFrom',
    'Module',
    'NamedExpr', 'Nonlocal',
    'Pass',
    'Raise',
    'Return',
    'Try',
    'While', 'With',
    'Yield', 'YieldFrom',
    # fmt: on
]

# Base64-encoded icon file
ICON: Final = b'iVBORw0KGgoAAAANSUhEUgAAABAAAAAQBAMAAADt3eJSAAAAElBMVEUAAAA3dqv/5I3XyJg3dqv/5I0E9bTlAAAABHRSTlMAgIDA8kEn4QAAACpJREFUCNdjYGBwYYACFwEYw8XFAcwwgTFCYWpCFWCM0NAAMEMVxqCKQQD7WguxCTtoqQAAAABJRU5ErkJggg=='


PathStr = Union[Path, str]


class AstBrowser(tkinter.Toplevel):
    icon: ClassVar[Optional[tkinter.PhotoImage]] = None

    tree: ttk.Treeview

    def __init__(self, root: tkinter.Tk, *, config: Dict[str, Any]):
        super().__init__(root)
        self.protocol('WM_DELETE_WINDOW', self.destroy)
        self.config = config

        geometry = config.get('geometry')
        if not geometry:
            width = max(400, int(self.winfo_screenwidth() * 0.4))
            height = max(600, int(self.winfo_screenheight() * 0.9))
            geometry = f'{width}x{height}'
        self.geometry(geometry)
        self.iconphoto(True, self.get_icon(self))
        self.title(APPNAME)

        yscroll = ttk.Scrollbar(self, orient='vertical')
        tree = self.tree = ttk.Treeview(
            self,
            columns=['line', 'col', 'dispnode'],
            displaycolumns=['line', 'col'],
            yscrollcommand=yscroll.set,
        )
        tree.heading('#0', text="Node")
        tree.heading('line', text="Line")
        tree.heading('col', text="Col")
        tree.column('line', anchor='center', stretch=False, width=50)
        tree.column('col', anchor='center', stretch=False, width=50)
        yscroll['command'] = tree.yview

        tree.pack(side='left', expand=True, fill='both')
        yscroll.pack(side='right', expand=False, fill='y')

        style = ttk.Style(self)
        font = config.get('font.ast')
        if font:
            style.configure('Treeview', font=font)
        background_func = self._get_background_func(style)

        tree.tag_configure('-message', font=config.get('font.message', 'TkFixedFont'))
        for tag in HIGHLIGHT_NODES:
            tree.tag_configure(tag, background=background_func(tag))

    @staticmethod
    def get_icon(master: tkinter.Misc) -> tkinter.PhotoImage:
        icon = AstBrowser.icon
        if not icon:
            icon = AstBrowser.icon = tkinter.PhotoImage(master=master, data=ICON)
        return icon

    def open_ast(self, astnode: Any, title: str, root_label: str = "root") -> None:
        self.title(f"{title} - {APPNAME}" if title else APPNAME)
        self._load_ast(astnode, root_label)

    def open_code(self, pycode: str, filename: str) -> None:
        if (pos := pycode.rstrip("\n").find("\n")) == -1:
            title = pycode
        else:
            title = f"{pycode[:pos]} ..."
        self.title(f"{title} - {APPNAME}")
        self._load_code(pycode, filename)

    def open_path(self, path: PathStr) -> None:
        path = Path(path)
        self.title(f"{path.name} ({path.parent.absolute()}) - {APPNAME}")
        pycode = path.read_text()
        self._load_code(pycode, str(path))

    def _add_tree_node(self, dispnode: DispNode, treeparent: str) -> None:
        tree = self.tree
        text = []
        if label := dispnode.label:
            text.append(f"{label} =")
        name = dispnode.name
        text.append(name)
        if name == 'list':
            text.append("()")
        else:
            attrs = [f"{key}={value}" for key, value in dispnode.inline_attrs.items()]
            text.append(f"({', '.join(attrs)})")
        if pos := dispnode.pos:
            lineno = str(pos[0])
            colno = str(pos[1] + 1)
        else:
            lineno = ""
            colno = ""
        treenode = tree.insert(
            treeparent,
            'end',
            text=" ".join(text),
            values=[lineno, colno, dispnode.label],
            open=True,
            tags=[name],
        )
        for child in dispnode.children:
            self._add_tree_node(child, treenode)

    def _get_background_func(self, style: ttk.Style) -> Callable[[str], str]:
        background = style.configure('Treeview', 'background')
        is_light_bg = any(x >> 15 for x in style.master.winfo_rgb(background))
        if is_light_bg:
            confname = 'light'
            luma = 0.9
        else:
            confname = 'dark'
            luma = 0.3
        return lambda tag: self.config.get(f'bgcolor.{confname}.tag') or get_color_string(
            *get_colorhash(tag, luma)
        )

    def _load_ast(self, astnode: Any, root_label: str) -> None:
        tree = self.tree
        tree.delete(*tree.get_children())
        root_dispnode = create_display_tree(root_label, astnode)
        if isinstance(astnode, list):
            for child in root_dispnode.children:
                self._add_tree_node(child, '')
        else:
            self._add_tree_node(root_dispnode, '')

    def _load_code(self, pycode: str, filename: str) -> None:
        try:
            if filename.endswith('.hy'):
                module = parse_hy(pycode, filename)
            else:
                module = ast.parse(pycode, filename)
        except Exception as ex:
            from traceback import format_exc

            self._show_message(format_exc())
        else:
            self._load_ast(module.body, "module.body")

        tree = self.tree
        children = tree.get_children()
        if children:
            tree.focus(children[0])
        tree.focus_set()

    def _show_message(self, text: str) -> None:
        tree = self.tree
        tree.delete(*tree.get_children())
        for line in text.rstrip("\r\n").split("\n"):
            tree.insert('', 'end', text=line, tags=['-message'])

        children = tree.get_children()
        if children:
            tree.focus(children[0])
        tree.focus_set()


@dataclass
class DispNode:
    pos: Optional[Tuple[int, int, Optional[int], Optional[int]]]
    label: str
    name: str
    inline_attrs: Dict[str, str]
    children: List[DispNode]
    parent: Optional[DispNode] = None

    def adjust_parents(self) -> None:
        """Recursively set the `parent` attribute (does not touch the current node)"""
        for child in self.children:
            child.parent = self
            child.adjust_parents()

    def get_path_label(self) -> str:
        labels = [self.label]
        node = self
        while parent := node.parent:
            node = parent
            labels.append(node.label)
        return "".join(reversed(labels))


def create_display_tree(label: str, astnode: Any) -> DispNode:
    pos: Optional[Tuple[int, int, Optional[int], Optional[int]]]
    inline_attrs = {}
    child_attrs: Dict[str, Any] = {}
    if isinstance(astnode, ast.AST):
        try:
            pos = (
                astnode.lineno,
                astnode.col_offset,
                astnode.end_lineno,
                astnode.end_col_offset,
            )
        except AttributeError:
            pos = None
        name = type(astnode).__name__
        for key, value in ast.iter_fields(astnode):
            if isinstance(value, list):
                if value:
                    child_attrs[f".{key}"] = value
                else:
                    # Inline empty lists
                    inline_attrs[key] = "[]"
            elif isinstance(value, ast.expr_context):
                # Inline expression contexts (e.g. ctx=Store())
                inline_attrs[key] = ast.dump(value, False, False)
            elif isinstance(value, ast.AST):
                child_attrs[f".{key}"] = value
            else:
                inline_attrs[key] = repr(value)
    elif isinstance(astnode, list):
        pos = None
        name = "list"
        for i, item in enumerate(astnode):
            child_attrs[f"[{i}]"] = item
    else:
        pos = None
        name = repr(astnode)
    children = [create_display_tree(key, value) for key, value in child_attrs.items()]
    dispnode = DispNode(
        pos=pos, label=label, name=name, inline_attrs=inline_attrs, children=children
    )
    dispnode.adjust_parents()
    return dispnode


def get_color_string(r: int, g: int, b: int) -> str:
    return f'#{r:02x}{g:02x}{b:02x}'


def get_colorhash(text: str, luma: float) -> Tuple[int, int, int]:
    """Get an arbitrary (but stable) color for a particular string using the specified luma"""

    import colorsys
    import hashlib

    rgb = (x / 255 for x in hashlib.blake2s(text.encode('utf-8'), digest_size=3).digest())
    _, i, q = colorsys.rgb_to_yiq(*rgb)
    r, g, b = (round(x * 255) for x in colorsys.yiq_to_rgb(luma, i, q))
    return r, g, b


def get_config_basedirs() -> Iterator[Path]:
    if sys.platform == 'win32':
        yield Path(os.environ['APPDATA'])
    elif sys.platform == 'darwin':
        yield Path("~/Library/Application Support").expanduser()
    else:
        if confhome := os.environ.get('XDG_CONFIG_HOME'):
            yield Path(confhome)
        else:
            yield Path("~/.config").expanduser()
        if confdirs := os.environ.get('XDG_CONFIG_DIRS'):
            yield from map(Path, confdirs.split(":"))
        else:
            yield Path("/etc/xdg")


def get_config_paths(basedirs: Iterable[Path]) -> Iterator[Path]:
    yield from (d / "pyastbrowser/config.ini" for d in basedirs)


def open_any(paths: Iterable[Path]) -> TextIO:
    """Open the first available file for reading"""
    for path in paths:
        try:
            return path.open(encoding='utf-8')
        except FileNotFoundError:
            pass
    raise FileNotFoundError(f"config file not found in {paths}")


def parse_hy(hycode: str, filename: str) -> ast.Module:
    from hy.compiler import calling_module, hy_compile, hy_parse

    model = hy_parse(hycode, filename)
    return hy_compile(model, calling_module())


def read_config(f: TextIO) -> Dict[str, Any]:
    """Read config file"""
    cp = ConfigParser(interpolation=None)
    cp.read_file(f)
    return {
        f'{secname}.{key}': ast.literal_eval(value)
        for secname, section in cp.items()
        for key, value in section.items()
    }


def run(path: Optional[PathStr]) -> None:
    tkinter.NoDefaultRoot()
    root = tkinter.Tk()
    root.withdraw()
    if path is None:
        from tkinter import filedialog

        root.iconphoto(True, AstBrowser.get_icon(root))
        path = filedialog.askopenfilename(parent=root)
    if not path:
        return

    confpaths = get_config_paths(get_config_basedirs())
    try:
        f = open_any(confpaths)
    except OSError:
        config = {}
    else:
        with f:
            config = read_config(f)

    browser = AstBrowser(root, config=config)
    browser.protocol('WM_DELETE_WINDOW', root.quit)
    if path == '-':
        browser.open_code(sys.stdin.read(), "<stdin>")
    else:
        browser.open_path(path)
    root.mainloop()


def _main() -> None:
    argv = sys.argv
    argc = len(argv)

    path: Optional[str]
    if argc == 1:
        path = None
    elif argc == 2:
        path = argv[1]
    else:
        print(f"Syntax: {argv[0]} [FILE|-]", file=sys.stderr)
        sys.exit(1)

    run(path)


if __name__ == '__main__':
    _main()
