# Python AST Browser


## Dependencies

* Python >= 3.8
* Tk and Tkinter (if not bundled with Python)
* Optional:
  * Hy: support for Hy code

Testing:

* pytest
* Optional:
  * Black
  * Hy
  * MyPy
  * pytest-cov


## Usage

* `pyastbrowser` lets you choose a file to read.
* `pyastbrowser -` reads standard input.
* `pyastbrowser FILE` reads the named file.


## Example

Reading from stdin (sample code from Python's [`functools.wraps`](https://docs.python.org/3/library/functools.html#functools.wraps) documentation):

```
$ pyastbrowser -
from functools import wraps
def my_decorator(f):
    @wraps(f)
    def wrapper(*args, **kwds):
        print('Calling decorated function')
        return f(*args, **kwds)
    return wrapper
^D
```

![Screenshot](https://gitlab.com/sjohannes/pyastbrowser/uploads/a12d6fd7bbd60b65ed3dd072a3f03745/screenshot.png)


## Limitations

* The Tk bundled with Python on Windows does not support row colors.
  It works fine on MSYS2 Python, so you may want to use that instead.
* Hy macros run at compile time, which means that it's not safe to run this program on untrusted `.hy` files.


## Contributing

By contributing to this project, you agree to release your contribution under the Apache License version 2.0.
