import pyastbrowser


def test_basic():
    module = pyastbrowser.parse_hy('(print "hello") 123', 'test.hy')
    assert len(module.body) == 2


def test_macro():
    pyastbrowser.parse_hy('(defmacro m [] (setv v 123)) (m)', 'test.hy')
